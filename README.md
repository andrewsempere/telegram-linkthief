# Link Thief

## Development

NOTE: This isn't working 'nodemon ./app/' instead
For development, uses docker-compose.yml

    yarn install
    docker-compose up

## Production Deploy

    ./deploy

Will use "Dockerfile" to build the app into a node container and push to gitlab repo. This should be picked up in cloud stack by watchtower.

## The Stack:

This whole thing probably lives under something like:

    ~/Code/Servers/CLOUD/stack-additional

This is what allows it to exist as a stack, [per this setup](https://gitlab.com/theplacelab/template-docker-backend/tree/master). There is a deploy script there as well, but you probably don't need to run it (updates to the app are handled by watchtower via the production deploy above)
