require("dotenv").config();
const express = require("express");
const Telegraf = require("telegraf");
const { reply } = Telegraf;
const Extra = require("telegraf/extra");
const session = require("telegraf/session");
const CommandHandler = require("./Bot/CommandHandler.js");
const commandHandler = new CommandHandler();
const KeywordHandler = require("./Bot/KeywordHandler.js");
const keywordHandler = new KeywordHandler();
const DatabaseMgr = require("./DatabaseManager");
const databaseMgr = new DatabaseMgr();

const sqlite3 = require("sqlite3").verbose();

const app = express();
const bodyParser = require("body-parser");
const jsonParser = bodyParser.json();
const urlencodedParser = bodyParser.urlencoded({ extended: false });

const port = process.env.WEBCLIENT_PORT;

// Database ////////////////////////////////////////////
let db = new sqlite3.Database("data/database.sqlite3");
let writeQueue = [];
databaseMgr.init(db);
let emptyWriteQueue = () => {
	if (writeQueue.length > 0) {
		let thisItem = writeQueue[0];
		databaseMgr.writeToDatabase(db, thisItem);
		writeQueue.shift();
	}
	setTimeout(emptyWriteQueue, 250);
};
setTimeout(emptyWriteQueue, 250);

// WWW ////////////////////////////////////////////////
require("./Router")(app, db, jsonParser);

// Bot ////////////////////////////////////////////////
const bot = new Telegraf(process.env.BOT_TOKEN);
bot.use(session());
try {
	bot.on("message", ctx => {
		if (typeof ctx.message.text !== "undefined") {
			if (ctx.message.text.charAt(0) == "/") {
				commandHandler.handle(ctx);
			} else {
				keywordHandler.handle(ctx, db, writeQueue, databaseMgr);
			}
		}
	});
} catch (error) {
	console.log(error);
}

bot.launch();

// Node ////////////////////////////////////////////////
app.listen(port, () =>
	console.log(`${process.env.BOT_NAME} listening on port ${port}!`)
);
process.on("SIGTERM", () => {
	console.info("Shutdown...");
	try {
		db.close();
		app.close();
	} catch (e) {}
});
